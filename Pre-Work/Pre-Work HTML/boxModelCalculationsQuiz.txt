Calculation Assignment

Total Height:
margin-top(20) + border-top(1) + padding-top(10) +
content-height(150) + padding-bottom(10) + border-bottom(1) + 
margin-bottom(20)
20+1+10+150+10+1+20 = 212px
Total Height = 212px

Total Width:
m-left(20) + b-left(1) + p-left(10) + c-width(400) + p-right(10) +
b-right(1) + m-right(20)
20+1+10+400+10+1+20 = 462px
Total Width = 462px

Browser Calculated Height:
b-top(1) + p-top(10) + c-height(150) + p-bottom(10) + b-bottom(1)
1+10+150+10+1 = 172px
Browser Calculated Height = 172px

Browser Calculated Width:
b-left(1) + p-left(10) + c-width(400) + p-right(10) + b-right(1)
1+10+400+10+1 = 422px
Browser Calculated Width = 422px

Since all of the margin, padding, and border elements are consistent all the way
around, one could simply double the measurements of one side and add the content 
height or width to get the total. I chose to write everything out long hand to make
my work easy to follow.